import re
import uuid
import logging
import sys

from json import loads

interesting_signals = {
    4: "SIGILL",
    5: "SIGTRAP",
    6: "SIGABRT",
    8: "SIGFPE",
    11: "SIGSEGV"
}

interesting_return_codes = dict()


class HarnessTemplate(Harness):
    _name = "Harness Template"

    def __init__(self):
        """
        Harness Template constructor.
        """
        super().__init__()
        self._pid = None

    @staticmethod
    def _parse_crash_reason_and_regs(buf, signal, return_code):
        """
        Parse crash log and find the reason and registers value.

        :param buf: crash log buffer
        :param signal: signal
        :returns: reason, registers dict
        """

        reason = None
        registers = dict()
        is_asan_crash = False
        is_interesting_crash = False
        crash_location = "N/A"
        if b"ERROR: AddressSanitizer" in buf:
            is_asan_crash = True
        elif signal in interesting_signals or return_code in interesting_return_codes:
            is_interesting_crash = True

        pattern_strict = b"ERROR: AddressSanitizer:\s" \
                         b"(.*)\s" \
                         b"on\s" \
                         b"(unknown address|address)\s" \
                         b"(0x[a-f0-9]+).*(pc)\s" \
                         b"(0x[a-f0-9]+).*(bp|sp)\s" \
                         b"(0x[a-f0-9]+).*(bp|sp)\s" \
                         b"(0x[a-f0-9]+)"

        pattern_lenient = b"ERROR: AddressSanitizer:\s(.*)[:|.*]"
        pattern_crash_location = b"ERROR: AddressSanitizer:\s(.*)"

        ret = re.findall(pattern_strict, buf, re.IGNORECASE)

        if not ret:
            # parse leniently
            ret = re.findall(pattern_lenient, buf, re.IGNORECASE)

            if not ret:
                return is_asan_crash, is_interesting_crash, reason, crash_location, registers
            else:
                crash_location_ret = re.findall(
                    pattern_crash_location, buf, re.IGNORECASE)
                crash_location = crash_location_ret[0]
                reason = ret[0]
                return is_asan_crash, is_interesting_crash, reason, crash_location, registers

        crash_location_ret = re.findall(
            pattern_crash_location, buf, re.IGNORECASE)
        crash_location = crash_location_ret[0]

        ret = ret[0]
        reason = ret[0]

        # address = int(ret[2], 16)

        registers["pc"] = int(ret[4], 16)

        if ret[5] == "bp":
            registers["bp"] = int(ret[6], 16)
        else:
            registers["sp"] = int(ret[6], 16)

        if ret[7] == "sp":
            registers["sp"] = int(ret[8], 16)
        else:
            registers["bp"] = int(ret[8], 16)

        return is_asan_crash, is_interesting_crash, reason, crash_location, registers

    @staticmethod
    def _parse_stack_backtrace(buf):
        pattern = b"#(\d+)\s(0[xX][0-9a-fA-F]+)(?:\sin\s|\s)(.*)"
        back_traces = re.findall(pattern, buf, re.IGNORECASE)

        current_frame_number = 0
        stack_backtrace = dict()

        for backtrace in back_traces:
            frame_number, ret_address, symbol = backtrace

            if int(frame_number) == current_frame_number:
                stack_backtrace[int(frame_number)] = {
                    "ret_address": int(ret_address, 16),
                    "symbol": symbol.strip()
                }
                current_frame_number += 1
            else:
                break

        return stack_backtrace

    
    def run(self, **kwargs):

        crash_data = dict()
        path_to_input = sys.argv[3] 
        path_to_app = sys.argv[1] 
        app_args =  sys.argv[2]
        if app_args:
            command = "{0} {1}".format(
                path_to_app, app_args.format(
                    FuzzedFilePath=path_to_input)
            )
        else:
            command = "{0} {1}".format(
                path_to_app, path_to_input)

        process = subprocess.run(cmd=command,capture_output=True)

        self._pid = process.pid

        if not self._pid:
            return crash_data

        # Print the stdout so that we know what's.
        # Going on during executing fuzz tests.
        print("===============STDOUT===============")
        print(process.stdout)
        print("====================================")
        print("===============STDERR===============")
        print(process.stderr)
        print("====================================")

        crash_log_buffer = process.stderr
        return_code = process.return_code
        signal = -return_code if return_code < 0 else 0

        is_asan_crash, is_interesting_crash, reason, crash_location, registers = self._parse_crash_reason_and_regs(
            crash_log_buffer, signal, return_code
        )

        if is_asan_crash:
            stack_backtrace = self._parse_stack_backtrace(crash_log_buffer)
            crash_data["architecture"] = "{0}_asan".format(
                get_machine_architecture())
            crash_data["registers"] = registers
            crash_data["backTrace"] = stack_backtrace
            crash_data["stackDump"] = "N/A"
            crash_data["location"] = crash_location
            crash_data["disassembly"] = "N/A"
            crash_data["signal"] = (11, "SIGSEGV")
            crash_data["log"] = crash_log_buffer

        elif is_interesting_crash:
            fake_random_registers = {"pc": uuid.uuid1().int >> 64}
            fake_random_stack_backtrace = dict()

            for i in range(15):
                fake_random_stack_backtrace[uuid.uuid1(
                ).int >> 64] = uuid.uuid1().int >> 64

            fake_crash_location = uuid.uuid1().int >> 64
            fake_crash_log_buffer = "The crash context is not available"

            crash_data["architecture"] = "{0}_asan".format(
                get_machine_architecture())
            crash_data["registers"] = fake_random_registers
            crash_data["backTrace"] = fake_random_stack_backtrace
            crash_data["stackDump"] = "N/A"
            crash_data["location"] = fake_crash_location
            crash_data["disassembly"] = "N/A"
            if signal in interesting_signals:
                crash_data["signal"] = (
                    signal, interesting_signals[signal])
            elif return_code in interesting_return_codes:
                crash_data["signal"] = (
                    return_code, interesting_return_codes[return_code])
            crash_data["log"] = fake_crash_log_buffer

        return crash_data
