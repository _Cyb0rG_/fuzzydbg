# Testing radamsa input generation

We have the following json file `test.json`.

```json
{
  "test": "test",
  "array": [
    "item1: foo",
    "item2: bar"
  ]
}
```

In order to more efficiently test for vulnerabilities like this, a Bash script can be used to automate the fuzzing process, including generating test cases, passing them to the target program and capturing any relevant output.

```sh
#!/bin/bash
while true; do
  radamsa test.json > input.txt
  jq . input.txt > /dev/null 2>&1
  if [ $? -gt 127 ]; then
    cp input.txt crash-`date +s%.%N`.txt
    echo "Crash found!"
  fi
done
```

`jq` has been used to ensure if json file is syntactically correct or not.

This script contains a while to make the contents loop repeatedly. Each time the script loops, Radamsa will generate a test case based on test.json and save it to input.txt.

The input.txt test case will then be run through `jq`, with all standard and error output redirected to /dev/null to avoid filling up the terminal screen.

Finally, the exit value of jq is checked. If the exit value is greater than 127, this is indicative of a fatal termination (a crash), then the input data is saved for review at a later date in a file named crash- followed by the current date in Unix seconds and nanoseconds.