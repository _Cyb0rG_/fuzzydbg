# Coverage with Dynamorio

When choosing a target program it’s best to have the source code available and to compile it with debug symbols. Drcov also works without source code, but in that case the results will miss some features like line coverage.

When having multiple inputs drcov will generate coverage data for every input file which can later be aggregated into a single report.

```sh
for i in `ls -1 inputs/`; do drrun -t drcov -- tests/test1.bin inputs/$i; done
```

Now that we have obtained the coverage data in lcov format, a html report can be generated using:

```sh
genhtml coverage.info --output-directory out
```

#  Coverage info painted to IDA

Their are situations where we are fed up of finding the control flow of the binary, so this idea has evoked at a similar situation. First we have to get the trace of the instructions of the binary, for this their are many ways to fetch the instructions using pintool or r2.

I have used gdb to get the trace of instructions by breaking at the address intended to debug and continue stepping into next instruction while the stdout will be logged into the file logfile.

```sh
import gdb

gdb.execute("file a.out")
gdb.execute("set disassembly-flavor intel")
gdb.execute("set pagination off")
gdb.execute("set logging file logfile")
gdb.execute("set logging on")

gdb.execute("b*main")
gdb.execute("r")
while(1):
gdb.execute("ni")
gdb.execute("x/s $rip")

```
Here the logfile contains the stdout of file a.out. So I only want the list of instructions, so I wrote a python script to fetch the instuctions alone and will write to another file instruction.

```py
log = open("logfile","r")
read_logfile = log.readlines()
addr = open("instructions","w")

for i in read_logfile:
    for j in i.split():
        if '0x' in j:
            try:
                int(j.strip(':'),16)
            except:
                continue
            addr.write(j.strip(':') + '\r\n')
```
Finally we are having the all the addresses in our file instructions. By exporting the address to ida using the IDApython script(below) we can colour the instructions in IDA.

```py
from idautils import *
from idc import *

f = open("shit","r")
addr = f.readlines()

for i in addr:
        SetColor(int(i,16), CIC_ITEM, 0x98ff000)
```
So our control flow is ready, Now we can only focus on the coloured instructions.
