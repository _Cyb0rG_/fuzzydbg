# FuzzyDbg

A debugger with fuzzing and execution trace support.

## Features Implemented

1. Support for x86 64 bit binaries.
2. Entirely written in C for performence.
3. Includes a simple mutation based fuzzer.
4. Debugger has functionalities for attach, single step, breakpoint, continue, elfinfo, fuzz.
5. Execution trace after every crash or exit.
6. Robust interface showing registers, disassembly and stack.

[![asciicast](https://asciinema.org/a/491289.svg)](https://asciinema.org/a/491289)

## Features yet to be Implemented

1. Make debugger robust to multi-threading.
2. Improve fuzz testing to include creating custom harnesses.
## Requirements

`Zydis` disassembler has been used to implement disassembly.
The build process has been documented on the [zydis](https://github.com/zyantific/zydis/) repo.

## Compilation

To compile the debugger, simply run make from inside the `/debugger/src` folder.

## Maintainers

- [Ritvik](https://twitter.com/_Cyb0rG)
- [Sandhra Bino](https://twitter.com/SandhraBino)
- [Vishnu Madhav](https://twitter.com/L4V450N)
- Vivek K

## White Paper

The [white paper](Debugger/assets/FuzzyDbg.pdf) can be used as a reference to understand more about the overview of the implementation of the debugger.
