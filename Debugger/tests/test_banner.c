#include <stdio.h>

char banner[] = "Welcome to FuzzyDbg \n\n-*Usage*-  \n" 
                "attach                      : Start a debugging session at entrypoint\n"
                "exec                        : Run the program normally\n"
                "break <func_name/address>   : Set breakpoint at function or address\n"
                "show  <b,r,c,w>             : Show breakpoints (b), registers (r), context (c)"
                                              "and watchpoints (w) \n"
                "c                           : Continue until the next breakpoint or signal\n";

int main(){
    printf("%s",banner);
}