#include <stdio.h>

int main(){
	__asm__(".intel_syntax noprefix;"
			"xor rax,rax;"
			"lea rsi,[rbp-0x40];"
			"mov rdx,0x20;"
			"int 0x80;"
			".att_syntax");
	return 0;
}
