#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void getBinBase(){
    char base[0x20];
    FILE* fp = fopen("/proc/6550/maps","r");
    assert(fp != NULL);
    fscanf(fp,"%32s[^-]",base);
    unsigned long binBase = strtoul(base,NULL,16);
    if(!binBase)
        printf("[-] Something's wrong while parsing the binary\n");
    else
        printf("binBase = 0x%lx\n",binBase);
}

int main(){
    getBinBase();
}