// how to compile: gcc dynamic_loading.c -o dynamic_loading -ldl
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
 
int main(int argc, char **argv) {
    void *handle;
    void (*go)(char *);
 
    // get a handle to the library that contains 'puts' function
    handle = dlopen ("/lib/x86_64-linux-gnu/libc.so.6", RTLD_LAZY);
 
    // reference to the dynamically-resolved function 'puts'
    go = dlsym(handle, "puts");
 	
	printf("puts : %p",go);
 
    // cleanup
    dlclose(handle);
}
