#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>

int main(){
	pid_t pid;
	printf("Parent pid : %u\n",getpid());
	switch(pid = fork()){
		case 0:
			printf("Child pid : %u\n",getpid());
			break;
		default:
			printf("Parent pid : %u\n",getpid());
			break;
	}
	return 0;
}
