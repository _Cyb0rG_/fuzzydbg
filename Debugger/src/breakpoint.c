#include "interface.h"

void *breakAddr = (void *)NULL;
struct breakpoint *breaktab;
/* 
 ** init_breaktab - Initialize the breakpoint table, one entry per breakpoint
 */                
void initBreakPointTable(){ 
    breaktab = (struct breakpoint *)malloc(sizeof(struct breakpoint)*MAX_BREAKS);
}

/*
 * setBreakPoint - Insert a 1-byte breakpoint instruction (\xcc -> debugger interrupt) at
 * location addr in the trace process, returning the original contents
 * of the 8-byte starting at that address.
 */

void setBreakPoint(ull addr){
    u64 old_word,new_word = 0;
    int entry = 0;
    // make sure if process is alive
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process attached\n" reset);
        return;
    }
    // make sure that address is not null
    if(!addr){
        info(BHRED "[-]" reset " Breakpoint address not given");
        return;
    }
    // TODO : Determine the validity of address
    // Find an empty entry in the breakpoint table
    for (entry=0; entry < MAX_BREAKS; entry++) {
	    if(breaktab[entry].addr == NULL){
            break;
        }
    }
    // check if the breakpoint being set is already a tracepoint 
    idx = getTraceIdx((void *)addr);
    if(idx >= 0){
        /* Record the breakpoint as the instruction is already patched */
        breaktab[entry].addr = (void *)addr;
        breaktab[entry].save = traceTable[idx].save;
        breakAddr = (void *)addr;
        info(BHBLU "[+]" reset " Breakpoint set at " BHRED "0x%llx\n" reset,addr);
        return;
    }
    if(entry == MAX_BREAKS){
        info(BHRED "[-]" reset " Max breakpoints limit reached\n" reset);
        return;
    }
    /* Save the original instruction into old_word */
    old_word = ptrace(PTRACE_PEEKTEXT, pid, addr, 0);
    sleep(0.5);
    checkPtrace();
    if(!old_word){
        info(BHRED "[-]" reset " Something's wrong while setting breakpoint");
        return;
    }
    // patching with \xcc - int3 debugger interrupt
    new_word = ((old_word & ~0xff) | 0xcc);
    ptrace(PTRACE_POKETEXT, pid, addr, new_word);
    sleep(0.5);
    checkPtrace();
    breaktab[entry].addr = (void *)addr;
    breaktab[entry].save = old_word;
    breakAddr = (void *)addr;
    info(BHBLU "[+]" reset " Breakpoint set at " BHRED "0x%llx\n" reset,addr);
}

/*
 * deleteBreakPoint - this is for deleting the software breakpoints set by user
 */
void deleteBreakPoint(int idx){
    if(idx < 0 || !breaktab[idx].addr || !breaktab[idx].save || idx > MAX_BREAKS){
        info(BHRED "[-]" reset " No such breakpoint\n" reset);
        return;
    }
    /* If any new breakpoints are set in the vicinity of the ones already set,
     * we do not take any chances, we ptrace the instruction at addr and patch it's 
     * last byte to saved instruction's value 
     */

    u64 inst = ptrace(PTRACE_PEEKTEXT,pid,breaktab[idx].addr,0);
    checkPtrace();
    inst = (inst & ~0xff) | (breaktab[idx].save & 0xff);
    ptrace(PTRACE_POKETEXT, pid, breaktab[idx].addr, inst);
    checkPtrace();
    // clear the breakpoint table
    breaktab[idx].addr = NULL;
    breaktab[idx].save = 0;
    breakAddr = (void *)NULL;
}

/* There are instances where we need to temporerily disable breakpoints 
** If attached process calls system calls like fork or execve , the debugger might crash
*/
 
void disableBreakPoint(u64 idx){
    if(!breaktab[idx].addr || !breaktab[idx].save || idx > MAX_BREAKS){
        info(BHRED "[-]" reset " No such breakpoint\n" reset);
        return;
    }
    u64 inst = ptrace(PTRACE_PEEKTEXT,pid,breaktab[idx].addr,0);
    checkPtrace();
    inst = (inst & ~0xff) | (breaktab[idx].save & 0xff);
    ptrace(PTRACE_POKETEXT, pid, breaktab[idx].addr,inst);
    checkPtrace();
}

/* 
 * enableAllBrkpts - Set all breakpoints in breakpoint table
 */                
void enableAllBrkpts(){ 
    u64 entry = 0;
    for (entry = 0; breaktab[entry].addr != NULL; entry++) 
	    setBreakPoint((ull)breaktab[entry].addr);
}

void clearAllBrkpts(){
    u64 entry;
    info(BHYEL "[*]" reset " Clearing any existing breakpoints\n");
    for (entry = 0; breaktab[entry].addr != NULL; entry++)
	    deleteBreakPoint(entry);
}

/* 
 * disableAllBrkpts - Clear all breakpoints in breakpoint array
 */                
void disableAllBrkpts(){ 
    u64 entry = 0;
    info(BHYEL "[*]" reset " Disabling all breakpoints");
    for (entry = 0; breaktab[entry].addr != NULL; entry++)
	    disableBreakPoint(entry);	
}

int getBreakIdx(void *addr) {
    u64 entry = 0;
    if(!addr) return -1;
    for (entry = 0; (breaktab[entry].addr != NULL) && (entry < MAX_BREAKS); entry++){
	    if (breaktab[entry].addr == addr)
	        return entry;
    }
    return -1;
}