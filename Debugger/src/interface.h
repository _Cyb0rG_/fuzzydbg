#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ptrace.h>
#include <sys/reg.h>
#include <sys/user.h>
#include <sys/signal.h>
#include <wait.h>
#include <sys/user.h>
#include <string.h>
#include <sys/reg.h>
#include <time.h>
#include <elf.h>
#include <assert.h>
#include <errno.h>
#include <syscall.h>
#include <sys/auxv.h>
#include <signal.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <limits.h>
#include <Zydis/Zydis.h>
#include <dlfcn.h>
/* For colors */
#include "colors.h"

/* For storing libc stuff */
extern char libcMap[50][400];


/* fdb-utils */

/* Lookup table based switch cases implementation to handle different user commands */

typedef struct { 
    char *key; 
    int val; 
} hashMap;

/* For stuff like mapping syscalls and signals to their names */
typedef struct{ 
    int key;
    char* val;
} syscallSignalMap;

/* Constants for implementing hash map based switch cases */

enum opts{
    ATTACH,
    EXEC,
    BREAK,
    DEL,
    SHOW,
    SINGLESTEP,
    FIN,
    ELFINFO,
    FUZZ,
    HELP,
    CONTINUE,
    RUN,
    QUIT = 0x1337
};

extern bool isProcessAttached;

extern hashMap cmdLookupTable[];
extern hashMap regLookupTable[];
extern syscallSignalMap syscallLookupTable[];
extern syscallSignalMap signalLookupTable[];
extern struct stat sbuf;
extern struct stat libcSbuf;

#define CMD_NKEYS     (sizeof(cmdLookupTable)/sizeof(hashMap))
#define REG_NKEYS     (sizeof(regLookupTable)/sizeof(hashMap))
#define SYSCALL_NVALS (sizeof(syscallLookupTable)/sizeof(syscallSignalMap))
#define SIGNAL_NVALS  (sizeof(signalLookupTable)/sizeof(syscallSignalMap))


#define BADKEY -11;

typedef unsigned long u64;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;
typedef unsigned long long ull;

extern pid_t pid;
extern int fd;
extern int libcFd;
extern u64 cmdSize;
extern char *outBuf;
extern char *cmdArgs[512];
extern char *procName;
extern u64 binBase;
extern u64 libcBase;
extern struct user_regs_struct regs;

/* Logging function */

#define info(...) printf(__VA_ARGS__);

#define err(...)             \
    do                       \
    {                        \
        perror(__VA_ARGS__); \
        exit(-1);            \
    } while (0);

/* Help Banner */

extern char banner[];


/* Formats for various proc files */

#define PROCAUXVFMT "/proc/%d/auxv"
#define PROCMAPSFMT	"/proc/%d/maps"
#define LIBCPATH    "/lib/x86_64-linux-gnu/libc.so.6"
#define PROCMAPS_LINE_MAX_LENGTH  (PATH_MAX + 100) 

/* Important helper functions */

void printBanner();

void initialize(char *fname);

void fdbPrint(char *msg);

char *fdbGetInp();

int cmdKeyFromString(char *key);

int regKeyFromString(char *key);

void safeQuit();

void splitCmdArgs(char *cmd);

void showRegs();

void showDisassembly();

void showStack();

u64 getValue(char *reg, u64 offset);

void showContext();

void readLibcHeader();

void readCmdOutput(char *cmd,u64 size);

u64 getRegs(char *reg);

void setRegs(char *reg,ull addr);

int isWriteable(void *p);

void checkPtrace();

void getBinBase();

void getLibcBase();

char *sysName(int sysnum);

char *signalName(int signalnum);

void sigINTHandler();

int cp(const char *to, const char *from);

/* ELF-utils */

extern Elf64_Ehdr *elfHeader;
extern Elf64_Shdr *shdr;

/* Structure to handle ELF parsing */
typedef struct {
    void *maddr;            /* Start of mapped Elf binary segment in memory */
    int mlen;               /* Length in bytes of the mapped Elf segment */
    Elf64_Ehdr *ehdr;       /* Start of main Elf header (same as maddr) */
    Elf64_Sym *staticSymbolTable;      /* Start of symbol table */
    Elf64_Sym *staticSymbolTableEnd;   /* End of symbol table (symtab + size) */
    char *staticStrTable;              /* Address of string table */
    Elf64_Sym *dynamicSymbolTable;     /* Start of dynamic symbol table */
    Elf64_Sym *dynamicSymbolTableEnd;  /* End of dynamic symbol table (dsymtab + size) */
    char *dynamicStrTable;          /* Address of dynamic string table */ 
} Elf_obj;

extern Elf_obj *elfPtr;
extern Elf_obj *libcPtr;

extern u64 entryPoint      ;
extern u64 entryPointOffset;

void readELFHeader64();

bool isELF64();

void showHeader64();

void readSectionHeaderTable();

bool isBinPIE();

char *staticSymName(Elf64_Sym *sym);

char *dynSymName(Elf64_Sym *sym);

Elf64_Sym *firstStaticSym();

Elf64_Sym *nextStaticSym(Elf64_Sym *sym);

Elf64_Sym *firstDynSym();

Elf64_Sym *nextDynSym(Elf64_Sym *sym);

bool isStaticFunc(Elf64_Sym *sym);

bool isDynFunc(Elf64_Sym *sym);

/* Returns pointer to the first symbol */
Elf64_Sym *elf_firstsym();

/* Debugger utils */

void context(bool regs,bool disas,bool stack);

int getSysnum();

int isFork(int sysnum);

void setPC(void *addr);

void *getPC();

void show();

void showMaps();

void disassemble(ull vaddr);

void fin();

/* Breakpoint-utils */

#define MAX_BREAKS 10

/* Every user declared breakpoint gets one of these */
struct breakpoint {
    void *addr;         /* Breakpoint address */          
    u64 save;           /* save the original instruction - used for patch */
};

extern struct breakpoint *breaktab;

extern void *breakAddr;

void initBreakPointTable();

void setBreakPoint(ull addr);

void disableBreakPoint(u64 idx);

void deleteBreakPoint(int idx);

void enableAllBrkpts();

/* temporarily disables all breakpoints (useful in case of new process fork) */
void disableAllBrkpts();

/* Permanently deletes all breakpoints from the breaktable */
void clearAllBrkpts();

int getBreakIdx(void *addr);

void singleStep();

/* Functionalities for attach */

/* Important global variables */

#define READAUXVALFMT "od -t d8 /proc/%d/auxv | awk 'NR ==10 {print $3}'"
#define PIDFROMPROCESSFMT "pgrep %s/fdb %s"
#define PROCESSFROMPIDFMT "pidof %s/fdb %s"
/* Boolean values and global variables to help distinguish between various debugger actions */
extern bool isBinDynamic     ;
extern bool isProcessPID     ;
extern bool isProcessName    ;
extern int idx;
static int firstEvent = 1;

/* Record events that occur on the traced child process */
struct event {
    enum {
	    EVENT_UNKNOWN,   
	    EVENT_NONE,
	    EVENT_SIGNAL,
	    EVENT_EXIT,
	    EVENT_EXIT_SIGNAL,
	    EVENT_BREAKPOINT
    } type;
    union {
	    int retval;      /* EVENT_EXIT */
	    int signum;      /* EVENT_SIGNAL, EVENT_EXIT_SIGNAL */
    } u;
};

enum state {
    STATE_UNDEF, 
    STATE_AWAITING_EXEC_BREAK,
    STATE_AWAITING_SOFTWARE_BREAK,
};

extern struct event event;

pid_t getPIDbyProcess(char *name,u64 size);

char *getProcessByPID(char *pid,u64 size);

void attach(char *pname);

void awaitEvent(struct event *eventp);

void processEvent(struct event *eventp);

void singleStep();

void cont(bool ok);

void checkPtrace();

void run();

void help();

/* Functions related to tracing */

#define MAX_TRACES 8192
#define HANDLE "/lib/x86_64-linux-gnu/libc.so.6"
struct funcTrace{
    u64 save;             /* Original word at the function start */
    void *addr;           /* Function address */
    Elf64_Sym *sym;       /* Symbol of function */
    enum {                /* Which symbol table is the symbol stored in? */ 
	    UNDEF,         /* Undefined */  
	    STATIC,        /* Static symbol table */
	    DYNAMIC        /* Dynamic symbol table */
    } symType;          
};

extern struct funcTrace traceTable[MAX_TRACES];
extern char *funcTraces[MAX_TRACES];

int getTraceIdx(void *addr);

void enableTracePoints(bool check);

void disableTracePoint(u64 idx);

void trace(bool check);

void showTrace();

void initTraceTable(bool check);

/* Functions related to fuzzing */


struct ORIGINAL_FILE {
    char * data;
    u64 length;
};

struct ORIGINAL_FILE getData(char* seed);

void mutateBits(int iteration,struct ORIGINAL_FILE origFile, size_t mutations);

void start(char **argv,int iteration);

void fuzz(char **argv);

