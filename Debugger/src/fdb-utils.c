#include "interface.h"

pid_t pid            = 0;
u64 cmdSize          = 0x10;
u64 binBase          = 0;
u64 libcBase         = 0;
char *outBuf;
char *cmdArgs[512]   = {NULL};
char *procName       = NULL;
struct user_regs_struct regs;
bool isProcessAttached = false;
struct stat sbuf;
char libcMap[50][400];
int libcFd;
    
char banner[] = BHGRN "\t\t\t\t-----------------------------Welcome to FuzzyDbg-------------------------------\n" reset                   
                BRED "\n\n\t\t\t---------------------------------------------Instructions------------------------------------------------------\n" reset              
                BRED "\t\t\t|" reset BHYEL "           attach                                 : " reset "\tStart a debugging session at entrypoint               " BRED "|\n" reset        
                BRED "\t\t\t|" reset BHYEL "           run                                    : " reset "\tRun the program without any breakpoints               " BRED "|\n" reset
                BRED "\t\t\t|" reset BHYEL "           elfinfo                                : " reset "\tShow information about the ELF                        " BRED "|\n" reset
                BRED "\t\t\t|" reset BHYEL "           ss                                     : " reset "\tSingle step through the binary                        " BRED "|\n" reset
                BRED "\t\t\t|" reset BHYEL "           break <address>                        : " reset "\tSet breakpoint at function or address                 " BRED "|\n" reset         
                BRED "\t\t\t|" reset BHYEL "           del                                    : " reset "\tDelete all breakpoints                                " BRED "|\n" reset         
                BRED "\t\t\t|" reset BHYEL "           show <state/regs/asm/stack/maps/trace> : " reset "\tShow state, registers, asm, memory maps or exec trace " BRED "|\n" reset                
                BRED "\t\t\t|" reset BHYEL "           cont                                   : " reset "\tContinue until the next breakpoint or signal          " BRED "|\n" reset
                BRED "\t\t\t|" reset BHYEL "           fuzz <seed> <iters>                    : " reset "\tFuzz the binary with desired seed and number of iters " BRED "|\n" reset
                BRED "\t\t\t|" reset BHYEL "           help                                   : " reset "\tDisplay instructions                                  " BRED "|\n" reset  
                BRED "\t\t\t---------------------------------------------------------------------------------------------------------------\n\n\x00" reset;           

void printBanner() { printf("%s\n" reset, banner); }

void initialize(char *fname){
    /* Prevent buffering */
    setvbuf(stdin, 0, _IONBF, 0);
    setvbuf(stdout, 0, _IONBF, 0);
    setvbuf(stderr, 0, _IONBF, 0);
    signal(SIGINT, sigINTHandler);
    if(fname != NULL){ 
        fd = open(fname,O_RDONLY);
        if(fd < 0) err(BHRED "[-]" reset "File could not be opened for reading\n");
    }
    if (fstat(fd, &sbuf) == -1) perror(BHRED "[-]" reset " stat\n");
    /* Also open the libc for parsing */
    libcFd = open(LIBCPATH,O_RDONLY);
    if(libcFd < 0) err(BHRED "[-]" reset " Libc could not be opened\n");
    if (fstat(libcFd, &libcSbuf) == -1) perror(BHRED "[-]" reset " stat\n");
    return;
}

void fdbPrint(char *msg) { printf(BHGRN "(fdb)> " reset  "%s", msg); }

hashMap cmdLookupTable[] = {
    { "attach", ATTACH  }, { "exec",   EXEC    }, { "break", BREAK } ,  { "del",  DEL },
    {"show", SHOW       }, {  "ss",  SINGLESTEP}, { "fin"  , FIN   } ,  {"elfinfo", ELFINFO}, 
    { "quit", QUIT      }, { "fuzz",  FUZZ     }, { "help",  HELP  }, 
    {"cont" , CONTINUE  }, { "run" , RUN}
};

hashMap regLookupTable[] = {
    {"rax", RAX}, {"rbx", RBX}, {"rcx",RCX},
    {"rdx",RDX},  {"rsi", RSI}, {"rdi",RDI},  
    {"r8",  R8},  {"r9",   R9}, {"r10",R10},
    {"r11",R11},  {"r12", R12}, {"r13",R13},
    {"r14",R14},  {"r15", R15}, {"rip",RIP},
    {"rsp",RSP}
};

syscallSignalMap syscallLookupTable[] = { 
{0 ,  "sys_read"},
{1 ,  "sys_write"},
{2 ,  "sys_open"},
{3 ,  "sys_close"},
{4 ,  "sys_stat"},
{5 ,  "sys_fstat"},
{6 ,  "sys_lstat"},
{7 ,  "sys_poll"},
{8 ,  "sys_lseek"},
{9 ,  "sys_mmap"},
{10,  "sys_mprotect"},
{11,  "sys_munmap"},
{12,  "sys_brk"},
{13,  "sys_rt_sigaction"},
{14,  "sys_rt_sigprocmask"},
{15,  "sys_rt_sigreturn"},
{16,  "sys_ioctl"},
{17,  "sys_pread64"},
{18,  "sys_pwrite64"},
{19,  "sys_readv"},
{20,  "sys_writev"},
{21,  "sys_access"},
{22,  "sys_pipe"},
{23,  "sys_select"},
{24,  "sys_sched_yield"},
{25,  "sys_mremap"},
{26,  "sys_msync"},
{27,  "sys_mincore"},
{28,  "sys_madvise"},
{29,  "sys_shmget"},
{30,  "sys_shmat"},
{31,  "sys_shmctl"},
{32,  "sys_dup"},
{33,  "sys_dup2"},
{34,  "sys_pause"},
{35,  "sys_nanosleep"},
{36,  "sys_getitimer"},
{37,  "sys_alarm"},
{38,  "sys_setitimer"},
{39,  "sys_getpid"},
{40,  "sys_sendfile"},
{41,  "sys_socket"},
{42,  "sys_connect"},
{43,  "sys_accept"},
{44,  "sys_sendto"},
{45,  "sys_recvfrom"},
{46,  "sys_sendmsg"},
{47,  "sys_recvmsg"},
{48,  "sys_shutdown"},
{49,  "sys_bind"},
{50,  "sys_listen"},
{51,  "sys_getsockname"},
{52,  "sys_getpeername"},
{53,  "sys_socketpair"},
{54,  "sys_setsockopt"},
{55,  "sys_getsockopt"},
{56,  "sys_clone"},
{57,  "sys_fork"},
{58,  "sys_vfork"},
{59,  "sys_execve"},
{60,  "sys_exit"},
{61,  "sys_wait4"},
{62,  "sys_kill"},
{63,  "sys_uname"},
{64,  "sys_semget"},
{65,  "sys_semop"},
{66,  "sys_semctl"},
{67,  "sys_shmdt"},
{68,  "sys_msgget"},
{69,  "sys_msgsnd"},
{70,  "sys_msgrcv"},
{71,  "sys_msgctl"},
{72,  "sys_fcntl"},
{73,  "sys_flock"},
{74,  "sys_fsync"},
{75,  "sys_fdatasync"},
{76,  "sys_truncate"},
{77,  "sys_ftruncate"},
{78,  "sys_getdents"},
{79,  "sys_getcwd"},
{80,  "sys_chdir"},
{81,  "sys_fchdir"},
{82,  "sys_rename"},
{83,  "sys_mkdir"},
{84,  "sys_rmdir"},
{85,  "sys_creat"},
{86,  "sys_link"},
{87,  "sys_unlink"},
{88,  "sys_symlink"},
{89,  "sys_readlink"},
{90,  "sys_chmod"},
{91,  "sys_fchmod"},
{92,  "sys_chown"},
{93,  "sys_fchown"},
{94,  "sys_lchown"},
{95,  "sys_umask"},
{96,  "sys_gettimeofday"},
{97,  "sys_getrlimit"},
{98,  "sys_getrusage"},
{99,  "sys_sysinfo"},
{100, "sys_times"},
{101, "sys_ptrace"},
{102, "sys_getuid"},
{103, "sys_syslog"},
{104, "sys_getgid"},
{105, "sys_setuid"},
{106, "sys_setgid"},
{107, "sys_geteuid"},
{108, "sys_getegid"},
{109, "sys_setpgid"},
{110, "sys_getppid"},
{111, "sys_getpgrp"},
{112, "sys_setsid"},
{113, "sys_setreuid"},
{114, "sys_setregid"},
{115, "sys_getgroups"},
{116, "sys_setgroups"},
{117, "sys_setresuid"},
{118, "sys_getresuid"},
{119, "sys_setresgid"},
{120, "sys_getresgid"},
{121, "sys_getpgid"},
{122, "sys_setfsuid"},
{123, "sys_setfsgid"},
{124, "sys_getsid"},
{125, "sys_capget"},
{126, "sys_capset"},
{127, "sys_rt_sigpending"},
{128, "sys_rt_sigtimedwait"},
{129, "sys_rt_sigqueueinfo"},
{130, "sys_rt_sigsuspend"},
{131, "sys_sigaltstack"},
{132, "sys_utime"},
{133, "sys_mknod"},
{134, "sys_uselib"},
{135, "sys_personality"},
{136, "sys_ustat"},
{137, "sys_statfs"},
{138, "sys_fstatfs"},
{139, "sys_sysfs"},
{140, "sys_getpriority"},
{141, "sys_setpriority"},
{142, "sys_sched_setparam"},
{143, "sys_sched_getparam"},
{144, "sys_sched_setscheduler"},
{145, "sys_sched_getscheduler"},
{146, "sys_sched_get_priority"},
{147, "sys_sched_get_priority"},
{148, "sys_sched_rr_get_inter"},
{149, "sys_mlock"},
{150, "sys_munlock"},
{151, "sys_mlockall"},
{152, "sys_munlockall"},
{153, "sys_vhangup"},
{154, "sys_modify_ldt"},
{155, "sys_pivot_root"},
{156, "sys__sysctl"},
{157, "sys_prctl"},
{158, "sys_arch_prctl"},
{159, "sys_adjtimex"},
{160, "sys_setrlimit"},
{161, "sys_chroot"},
{162, "sys_sync"},
{163, "sys_acct"},
{164, "sys_settimeofday"},
{165, "sys_mount"},
{166, "sys_umount2"},
{167, "sys_swapon"},
{168, "sys_swapoff"},
{169, "sys_reboot"},
{170, "sys_sethostname"},
{171, "sys_setdomainname"},
{172, "sys_iopl"},
{173, "sys_ioperm"},
{174, "sys_create_module"},
{175, "sys_init_module"},
{176, "sys_delete_module"},
{177, "sys_get_kernel_syms"},
{178, "sys_query_module"},
{179, "sys_quotactl"},
{180, "sys_nfsservctl"},
{181, "sys_getpmsg"},
{182, "sys_putpmsg"},
{183, "sys_afs_syscall"},
{184, "sys_tuxcall"},
{185, "sys_security"},
{186, "sys_gettid"},
{187, "sys_readahead"},
{188, "sys_setxattr"},
{189, "sys_lsetxattr"},
{190, "sys_fsetxattr"},
{191, "sys_getxattr"},
{192, "sys_lgetxattr"},
{193, "sys_fgetxattr"},
{194, "sys_listxattr"},
{195, "sys_llistxattr"},
{196, "sys_flistxattr"},
{197, "sys_removexattr"},
{198, "sys_lremovexattr"},
{199, "sys_fremovexattr"},
{200, "sys_tkill"},
{201, "sys_time"},
{202, "sys_futex"},
{203, "sys_sched_setaffinity"},
{204, "sys_sched_getaffinity"},
{205, "sys_set_thread_area"},
{206, "sys_io_setup"},
{207, "sys_io_destroy"},
{208, "sys_io_getevents"},
{209, "sys_io_submit"},
{210, "sys_io_cancel"},
{211, "sys_get_thread_area"},
{212, "sys_lookup_dcookie"},
{213, "sys_epoll_create"},
{214, "sys_epoll_ctl_old"},
{215, "sys_epoll_wait_old"},
{216, "sys_remap_file_pages"},
{217, "sys_getdents64"},
{218, "sys_set_tid_address"},
{219, "sys_restart_syscall"},
{220, "sys_semtimedop"},
{221, "sys_fadvise64"},
{222, "sys_timer_create"},
{223, "sys_timer_settime"},
{224, "sys_timer_gettime"},
{225, "sys_timer_getoverrun"},
{226, "sys_timer_delete"},
{227, "sys_clock_settime"},
{228, "sys_clock_gettime"},
{229, "sys_clock_getres"},
{230, "sys_clock_nanosleep"},
{231, "sys_exit_group"},
{232, "sys_epoll_wait"},
{233, "sys_epoll_ctl"},
{234, "sys_tgkill"},
{235, "sys_utimes"},
{236, "sys_vserver"},
{237, "sys_mbind"},
{238, "sys_set_mempolicy"},
{239, "sys_get_mempolicy"},
{240, "sys_mq_open"},
{241, "sys_mq_unlink"},
{242, "sys_mq_timedsend"},
{243, "sys_mq_timedreceive"},
{244, "sys_mq_notify"},
{245, "sys_mq_getsetattr"},
{246, "sys_kexec_load"},
{247, "sys_waitid"},
{248, "sys_add_key"},
{249, "sys_request_key"},
{250, "sys_keyctl"},
{251, "sys_ioprio_set"},
{252, "sys_ioprio_get"},
{253, "sys_inotify_init"},
{254, "sys_inotify_add_watch"},
{255, "sys_inotify_rm_watch"},
{256, "sys_migrate_pages"},
{257, "sys_openat"},
{258, "sys_mkdirat"},
{259, "sys_mknodat"},
{260, "sys_fchownat"},
{261, "sys_futimesat"},
{262, "sys_newfstatat"},
{263, "sys_unlinkat"},
{264, "sys_renameat"},
{265, "sys_linkat"},
{266, "sys_symlinkat"},
{267, "sys_readlinkat"},
{268, "sys_fchmodat"},
{269, "sys_faccessat"},
{270, "sys_pselect6"},
{271, "sys_ppoll"},
{272, "sys_unshare"},
{273, "sys_set_robust_list"},
{274, "sys_get_robust_list"},
{275, "sys_splice"},
{276, "sys_tee"},
{277, "sys_sync_file_range"},
{278, "sys_vmsplice"},
{279, "sys_move_pages"},
{280, "sys_utimensat"},
{281, "sys_epoll_pwait"},
{282, "sys_signalfd"},
{283, "sys_timerfd_create"},
{284, "sys_eventfd"},
{285, "sys_fallocate"},
{286, "sys_timerfd_settime"},
{287, "sys_timerfd_gettime"},
{288, "sys_accept4"},
{289, "sys_signalfd4"},
{290, "sys_eventfd2"},
{291, "sys_epoll_create1"},
{292, "sys_dup3"},
{293, "sys_pipe2"},
{294, "sys_inotify_init1"},
{295, "sys_preadv"},
{296, "sys_pwritev"},
{297, "sys_rt_tgsigqueueinfo"},
{298, "sys_perf_event_open"},
{299, "sys_recvmmsg"},
{300, "sys_fanotify_init"},
{301, "sys_fanotify_mark"},
{302, "sys_prlimit64"},
{303, "sys_name_to_handle_at"},
{304, "sys_open_by_handle_at"},
{305, "sys_clock_adjtime"},
{306, "sys_syncfs"},
{307, "sys_sendmmsg"},
{308, "sys_setns"},
{309, "sys_getcpu"},
{310, "sys_process_vm_readv"},
{311, "sys_process_vm_  writev"},
{312, "sys_kcmp"},
{313, "sys_finit_module"},
{314, "sys_sched_setattr"},
{315, "sys_sched_getattr"},
{316, "sys_renameat2"},
{317, "sys_seccomp"},
{318, "sys_getrandom"},
{319, "sys_memfd_create"},
{320, "sys_kexec_file_load"},
{321, "sys_bpf"},
{322, "stub_execveat"},
{323, "userfaultfd"},
{324, "membarrier"},
{325, "mlock2"},
{326, "copy_file_range"},
{327, "preadv2"},
{328, "pwritev2"},
{329, "pkey_mprotect"},
{330, "pkey_alloc"},
{331, "pkey_free"},
{332, "statx"},
{333, "io_pgetevents"},
{334, "rseq"},
{335, "pkey_mprotect"}
};

syscallSignalMap signalLookupTable[] = { 
{0	,"SIG_0"},
{1	,"SIGHUP"},
{2	,"SIGINT"},
{3	,"SIGQUIT"},
{4	,"SIGILL"},
{5	,"SIGTRAP"},
{6	,"SIGABRT"},
{7	,"SIGBUS"},
{8	,"SIGFPE"},
{9	,"SIGKILL"},
{10	,"SIGUSR1"},
{11	,"SIGSEGV"},
{12	,"SIGUSR2"},
{13	,"SIGPIPE"},
{14	,"SIGALRM"},
{15	,"SIGTERM"},
{16	,"SIGSTKFLT"},
{17	,"SIGCHLD"},
{18	,"SIGCONT"},
{19	,"SIGSTOP"},
{20	,"SIGTSTP"},
{21	,"SIGTTIN"},
{22	,"SIGTTOU"},
{23	,"SIGURG"},
{24	,"SIGXCPU"},
{25	,"SIGXFSZ"},
{26	,"SIGVTALRM"},
{27	,"SIGPROF"},
{28	,"SIGWINCH"},
{29	,"SIGIO"},
{30	,"SIGPWR"},
{31	,"SIGSYS"},
{32	,"SIGRTMIN"},
};

/* A dirty way of taking inputs without a length constraint */

char *fdbGetInp(){
    fdbPrint("");
    cmdSize = 0x10;
    int ch;
    u64 len = 0;
    /* Reading a variable length input */
    outBuf = calloc(1,cmdSize);
    if (!outBuf)
        err(BHRED "[-]" reset " Unable to allocate memory ,Something's wrong\n" reset);

    while (EOF != (ch = fgetc(stdin)) && ch != '\n'){
        outBuf[len++] = ch;
        if (len == cmdSize){
            outBuf = realloc(outBuf, cmdSize+=cmdSize*2);
            if (!outBuf)
                err(BHRED "[-]" reset " Unable to allocate memory ,Something's wrong\n" reset);
        }
    }
    outBuf[len++] = '\0';
    splitCmdArgs(outBuf);

    if (cmdArgs[0])
        return cmdArgs[0];
    else
        return (char *)NULL;
}

void checkPtrace(){
    if(errno == EPERM || errno == ESRCH || errno == EIO || errno == EFAULT || errno == EINVAL)
        err(BHRED "[-]" reset " Something's wrong in ptrace\n" reset);
}

int cmdKeyFromString(char *key){
    for (int i = 0; i < CMD_NKEYS ; i++){
        hashMap sym = cmdLookupTable[i];
        if (strcmp(sym.key, key) == 0)
            return sym.val;
    }
    return BADKEY;
}

int regKeyFromString(char *key){
    for (int i = 0; i < REG_NKEYS ; i++){
        hashMap sym = regLookupTable[i];
        if (strcmp(sym.key, key) == 0)
            return sym.val;
    }
    return BADKEY;
}

char *sysName(int key){
    if(key < 0 || key > SYSCALL_NVALS)
        return (char *)NULL;
    for (int i = 0; i < SYSCALL_NVALS ; i++){
        syscallSignalMap sym = syscallLookupTable[i];
        if (sym.key == key)
            return sym.val;
    }
    return (char *)NULL;
}

char *signalName(int key){
    if(key < 0 || key > SIGNAL_NVALS)
        return (char *)NULL;
    for (int i = 0; i < SIGNAL_NVALS ; i++){
        syscallSignalMap sym = signalLookupTable[i];
        if (sym.key == key)
            return sym.val;
    }
    return (char *)NULL;
}

/* Handle ctrl + c - TODO : Complete implementation for handling during blocking syscalls */

void sigINTHandler(){
    signal(SIGINT, sigINTHandler);
    fflush(stdout);
    fflush(stdin);
    return;
}

void safeQuit() { exit(EXIT_SUCCESS); }

/* Read output of a passed command - Resorting to a dumb implementation for now */

void readCmdOutput(char *cmd,u64 size){
    FILE *fp = popen(cmd, "r");
    assert(fp > 0);
    outBuf = (char *)malloc(size);  
    if(fgets(outBuf,size,fp) == NULL){
        err(BHRED "[-]" reset " something's wrong while reading from command");
    }
    return;
}

// break 0xdeadbeef 0xcafebabe

void splitCmdArgs(char *cmd){
    if (cmdSize > 512 * 8){
        info(BHRED "[-]" reset " Too many args to process");
        return;
    }
    if (!cmd) return;
    else if(cmd[0] == '\0') return;
    else{
        memset(cmdArgs,0,sizeof cmdArgs);
        // start the splitting
        char *token = strtok(cmd, " ");
        // loop through the string to extract all other tokens
        int i = 0;
        while (token != NULL){
            cmdArgs[i] = token;
            token = strtok(NULL, " ");
            i++;
        }
    }
}

/* 
 * getRegs - Fetch a register for the traced process
 */
u64 getRegs(char *reg) {
    struct stat sts;
    errno = 0;
    ptrace(PTRACE_GETREGS, pid, 0, &regs);
    checkPtrace();
    switch(regKeyFromString(reg)){
        case RAX:
            return regs.rax;
        case RBX:
            return regs.rbx;
        case RCX:
            return regs.rcx;
        case RDX:
            return regs.rdx;
        case RDI:
            return regs.rdi;
        case RSI:
            return regs.rsi;
        case R8:
            return regs.r8;
        case R9:
            return regs.r9;
        case R10:
            return regs.r10;
        case R11:
            return regs.r11;
        case R12:
            return regs.r12;
        case R13:
            return regs.r13;
        case R14:
            return regs.r14;
        case R15:
            return regs.r15;
        case RSP:
            return regs.rsp;
        case RIP:
            return regs.rip;
        default:
            break;
    }
}

/*
 * setRegs - Modify a register for the traced process
 */
void setRegs(char *reg,ull addr){
    switch(regKeyFromString(reg)){
        case RAX:
            regs.rax = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case RBX:
            regs.rbx = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case RCX:
            regs.rcx = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case RDX:
            regs.rdx = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case RDI:
            regs.rdi = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case RSI:
            regs.rsi = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R8:
            regs.r8 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R9:
            regs.r9 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R10:
            regs.r10 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R11:
            regs.r11 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R12:
            regs.r12 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R13:
            regs.r13 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R14:
            regs.r14 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case R15:
            regs.r15 = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case RSP:
            regs.rsp = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        case RIP:
            regs.rip = addr;
            ptrace(PTRACE_SETREGS, pid, 0, &regs);
            break;
        default:
            break;
        checkPtrace();
    }
}

int isWriteable(void *p){
    int fd = open("/dev/zero", O_RDONLY);
    int writeable;
    if (fd < 0)
        err(BHRED "[-]" reset " Something's wrong reading /dev/zero");

    writeable = read(fd, p, 1) == 1;
    close(fd);

    return writeable;
}

void getBinBase(){
    char cmd[0x40];
    char base[0x20];
    snprintf(cmd,0x40,PROCMAPSFMT,pid);
    FILE* fp = fopen(cmd,"r");
    assert(fp != NULL);
    fscanf(fp,"%32s[^-]",base);
    binBase = strtoul(base,NULL,16);
    if(!binBase)
        printf(BHRED "[-]" reset " Something's wrong while parsing the binary\n" reset);
    else
        printf("[*] binBase = 0x%lx\n" reset,binBase);
}

void getLibcBase(){
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process attached\n");
    }
    int i = 0;
    char cmd[0x40];
    char **endptr = NULL;
    snprintf(cmd,0x40,PROCMAPSFMT,pid);
    FILE* map = fopen(cmd,"r");
    assert(map != NULL);
    char *data = malloc(0x4000);
    while(!feof(map)){
        if(fgets(data,PROCMAPS_LINE_MAX_LENGTH,map) == NULL)
            err(BHRED "[-]" reset " Something wrong in parsing maps\n");
        if(strstr(data,"libc")){
            libcBase = strtoul(data,endptr,16);
            assert(libcBase != 0);
            info(BHBLU "[+]" reset " Libc base : " BHYEL "0x%lx\n\n" reset,libcBase);
            return;
        }
    } 

}

int cp(const char *to, const char *from){
    int fdTo, fdFrom = 0;
    char *data = (char *)malloc(4096);
    ssize_t nread = 0;
    int savedErrno;

    fdFrom = open(from, O_RDONLY);
    if (fdFrom < 0)
        return -1;

    fdTo = open(to, O_WRONLY | O_CREAT | O_EXCL, 0666);
    if (fdTo < 0)
        goto outError;

    while (nread = read(fdFrom, data, sizeof data), nread > 0){
        char *data = data;
        ssize_t nwritten;

        do {
            nwritten = write(fdTo, data, nread);

            if (nwritten >= 0){
                nread -= nwritten;
                data += nwritten;
            }
            else if (errno != EINTR) goto outError;
            
        } while (nread > 0);
    }

    if (nread == 0){
        if (close(fdTo) < 0){
            fdTo = -1;
            goto outError;
        }
        close(fdFrom);
        /* Success! */
        return 0;
    }

  outError:
    savedErrno = errno;
    close(fdFrom);
    if (fdTo >= 0) close(fdTo);
    errno = savedErrno;
    return -1;
}