#!/usr/bin/env python

from pwn import *

re = lambda a: io.recv(a)
reu = lambda a: io.recvuntil(a)
rl = lambda: io.recvline()
s = lambda a: io.send(a)
sl = lambda a: io.sendline(a)
sla = lambda a,b: io.sendlineafter(a,b)
sa = lambda a,b: io.sendafter(a,b)

context.terminal = ['tmux', 'splitw', '-h']
context.arch = "amd64"
context.aslr = False

brkpts = '''
b attach
'''


if __name__ == "__main__":
    io = gdb.debug("./fdb",brkpts)
    sla("(fdb)> ","attach")
    io.interactive()
