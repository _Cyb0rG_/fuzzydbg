#include "interface.h"

struct funcTrace traceTable[MAX_TRACES];
char *funcTraces[MAX_TRACES];

void initTraceTable(bool check){
    void *symAddr;
    Elf64_Sym *sym;

    /* Creating trace points for statically loaded functions */
    static int entry = 0;
    if(check){
        for(sym = firstStaticSym(); sym != NULL; sym = nextStaticSym(sym)){
            if(isStaticFunc(sym)){
                symAddr = (void *)sym->st_value;
                traceTable[entry].addr = symAddr;
                traceTable[entry].save = ptrace(PTRACE_PEEKTEXT, pid, symAddr, 0);
                sleep(0.5);
                checkPtrace();
                traceTable[entry].sym = sym;
                traceTable[entry].symType = STATIC;
                entry++;
            }
        }
    }
    else{
        /* Grab the required dynamic symbol addresses from libc */
        char *dynStr = elfPtr->dynamicStrTable;
        for(sym = firstDynSym(); sym != NULL; sym = nextDynSym(sym)){
            symAddr = (void *)sym->st_value;
            char *name = libcPtr->dynamicStrTable + sym->st_name;
            while(true){
                if(dynStr[0] == '\0'){
                    dynStr++;
                    continue;
                }
                else if(!strcmp(dynStr,"libc.so.6")){
                    dynStr += strlen("libc.so.6");
                    continue;
                }
                else if(!strncmp(dynStr,"GLIBC",strlen("GLIBC"))){
                    /* reset back dynStr for parsing again */
                    dynStr = elfPtr->dynamicStrTable;
                    break;
                }
                else{
                    if(!strcmp(dynStr,name)){
                        traceTable[entry].addr = symAddr + libcBase;
                        traceTable[entry].sym = sym;
                        traceTable[entry].save = ptrace(PTRACE_PEEKTEXT, pid, traceTable[entry].addr , 0);
                        traceTable[entry].symType = DYNAMIC;
                        entry++;
                    }
                    dynStr += strlen(dynStr);
                }
            }
        }
    }
}

void enableTracePoint(u64 idx,void *addr){
    u64 old_word, new_word = 0;
    // make sure if process is alive
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process attached\n" reset);
        return;
    }
    // make sure that address is not null
    if(!addr){
        info(BHRED "[-]" reset " Tracepoint address not given");
        return;
    }

    /* Save the original instruction into old_word */
    old_word = traceTable[idx].save;
    // patching with 0xcc : SIGTRAP
    new_word = ((old_word & ~0xff) | 0xcc);
    ptrace(PTRACE_POKETEXT, pid, addr, new_word);
    checkPtrace();
    return;
}

void enableTracePoints(bool check){
    /* Now enable all trace points */
    static int entry = 0;
    if(check){
        info(BHYEL "[*]" reset " Enabling all static tracepoints\n" reset);
        for(entry = 0; traceTable[entry].addr != NULL; entry++){
            enableTracePoint(entry,traceTable[entry].addr);
        }
    }
    else{
        info(BHYEL "[*]" reset " Enabling all dynamic tracepoints\n" reset);
        for(int i = entry; traceTable[i].addr != NULL; i++){
            enableTracePoint(i,traceTable[i].addr);
        }
    }
}

int getTraceIdx(void *addr){
    for(int entry = 0; traceTable[entry].addr != NULL; entry++){
        if(traceTable[entry].addr == addr){
            return entry;
        }
    }
    return -1;
}

void disableTracePoint(u64 idx){
    static int counter = 0;
    if(!traceTable[idx].addr || !traceTable[idx].save){
        info(BHRED "[-]" reset " No such tracepoint\n" reset);
        return;
    }
    info(BHYEL "[*]" reset " Recording a tracepoint\n\n");
    /* We know that this function has been hit, save it's symbol name */
    if(traceTable[idx].symType == STATIC)
        funcTraces[counter++] = (char *)elfPtr->staticStrTable + traceTable[idx].sym->st_name;
    else
        funcTraces[counter++] = (char *)libcPtr->dynamicStrTable + traceTable[idx].sym->st_name;
    /* Patch things back to how they were */
    u64 inst = ptrace(PTRACE_PEEKTEXT,pid,traceTable[idx].addr,0);
    checkPtrace();
    inst = (inst & ~0xff) | (traceTable[idx].save & 0xff);
    ptrace(PTRACE_POKETEXT, pid, traceTable[idx].addr, inst);
    sleep(0.5);
    checkPtrace();
    return;
}

void trace(bool check){
    if(check){
        initTraceTable(check);
        enableTracePoints(check);
    }
    else{
        initTraceTable(check);
        enableTracePoints(check);
    }
}

void showTrace(){
    info("\n\n----------------------------------------");
    info(BHCYN "TRACE" reset "-------------------------------------\n" reset);
    for(int i=0;i<MAX_TRACES;i++){
        if(funcTraces[i]){
            u64 size = strlen(funcTraces[i]) + 7;
            char *buf = (char *)malloc(size);
            memset(buf,45,size);
            *(buf + size) = '\0';
            info("\t\t\t\t    %s\n",buf);
            info("\t\t\t\t    |  %s   |\n",funcTraces[i]);
            info("\t\t\t\t    %s\n",buf);
            memset(buf,0,size);
            free(buf);
            if(funcTraces[i+1]){
                info("\t\t\t\t\t    |\n\t\t\t\t\t    |\n\t\t\t\t\t    |\n\t\t\t\t\t    |\n");
                info("\t\t\t\t\t    " BHRED "⇓\n" reset);
            }
        }
    }
    info("\n\n----------------------------------------------------------------------------------\n");
}