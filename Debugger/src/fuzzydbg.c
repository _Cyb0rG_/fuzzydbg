#include "interface.h"

void help(){
    printBanner();
}

int main(int argc,char *argv[]){
    initialize(argv[1]);
    char **endptr = NULL;
    printBanner();
    if(argc == 2){    
        readELFHeader64();
        readLibcHeader();
        while(true){
            char *cmd = fdbGetInp();
            // check for EOF (ctrl + d)
            if (feof(stdin)) err("\n[-] Debugger killed abruptly");
            if(!cmd) continue;
            switch(cmdKeyFromString(cmd)){
                case ATTACH:
                    attach(argv[1]);
                    break;
                case BREAK:
                    if(!cmdArgs[1]){
                        info(BHRED "[-]" reset " break <addr>\n" reset);
                        break;
                    }
                    breakAddr = (void *)strtoul(cmdArgs[1],endptr,16);
                    setBreakPoint((ull)breakAddr);
                    break;
                case DEL:
                    clearAllBrkpts();
                    break;
                case SHOW:
                    show();
                    break;
                case SINGLESTEP:
                    singleStep();
                    break;
                case FIN:
                    fin();
                    break;
                case ELFINFO:
                    showHeader64();
                    break;
                case CONTINUE:
                    cont(true);
                    break;
                case RUN:
                    run();
                    break;
                case FUZZ:
                    fuzz(argv);
                    break;
                case HELP:
                    help();
                    break;
                case QUIT:
                    safeQuit();
                    break;
                case EOF:
                default:
                    fdbPrint("Command not found\n" reset);
            }
        }
    }
    else{
        while(true){
            char *cmd = fdbGetInp();
            if (feof(stdin)) err("\n[-] Debugger killed abruptly");
            if(!cmd) continue;
            switch(cmdKeyFromString(cmd)){
                case QUIT:
                    safeQuit();
                    break;
                default:
                    fdbPrint("No process/file available for debugging\n" reset);
                    break;
            }
        }
    }
}
