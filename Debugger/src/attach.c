#include "interface.h"

bool isBinUnstripped = false;
bool isBinDynamic = false;
bool isProcessPID = false;
bool isProcessName = false;
int idx = 0;
struct event event;

pid_t gePIDbyProcess(char *name,u64 size){
    if (strtoul(name, NULL, 10) != EINVAL || strtoul(name, NULL, 10) != 0){
        char *cmd = (char *)malloc(strlen(name) + 0x10);
        errno = 0;
        // Going dumb here
        char *path = (char *)malloc(200);
        assert(getcwd(path,200) != NULL);
        snprintf(cmd, sizeof PIDFROMPROCESSFMT + strlen(path) + 
                    strlen(name), PIDFROMPROCESSFMT ,path,name);
        readCmdOutput(cmd,size);
        pid_t pid = strtoul(outBuf, NULL, 10);

        if (pid == EINVAL)
            err(BHRED "[-]" reset " Something's wrong on reading PID\n" reset);
        isProcessName = true;
    }
    else{
        pid = strtoul(name, NULL, 10);
        if (pid == EINVAL)
            err(BHRED "[-]" reset " Something went wrong processing PID\n" reset);
        isProcessPID = true;
    }
    return pid;
}

char *getProcessByPID(char *pname,u64 size){
    char *cmd = (char *)malloc(strlen(pname) + 0x10);
    errno = 0;
    // Going dumb here
    char *path = (char *)malloc(200);
    assert(getcwd(path,200) != NULL);
    snprintf(cmd,sizeof PROCESSFROMPIDFMT + strlen(path) + 
                strlen(pname),PROCESSFROMPIDFMT,path,pname);
    readCmdOutput(cmd,size);
    if (!outBuf)
        return (char *)NULL;
    return outBuf;
}

void singleStep(){
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process attached\n" reset);
        return;
    }
    ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
    sleep(0.7);
    checkPtrace();
    awaitEvent(&event);
    processEvent(&event);
}

void cont(bool ok){
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process Attached\n" reset);
        return;
    }
    ptrace(PTRACE_CONT, pid, 0, 0);
    sleep(0.4);
    checkPtrace();
    if(ok)
        info(BHYEL "[*]" reset " Continuing until an event\n" reset);
    awaitEvent(&event);
    processEvent(&event);
}

void run(){
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process Attached\n" reset);
        return;
    }
    clearAllBrkpts();
    ptrace(PTRACE_CONT, pid, 0, 0);
    sleep(0.5);
    checkPtrace();
    info(BHYEL "[*]" reset " Continuing until terminating signal\n" reset);
    awaitEvent(&event);
    processEvent(&event);
}

void fin(){
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process Attached\n" reset);
        return;
    }
    while(true){
        uint8_t opcode = ptrace(PTRACE_PEEKTEXT,pid,getPC(),0);
        checkPtrace();
        /* If we detect any of ret or it's counterparts, we have reached the end */
        if(opcode == 0xc3 || opcode == 0xc2 || opcode == 0xca || opcode == 0xcb){
            break;
        }
        /* Keep single stepping until we finish */
        ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
        sleep(0.7);
        checkPtrace();
        awaitEvent(&event);
        if (event.type == EVENT_BREAKPOINT) {
            /* event is most likely due to single step, we simply continue 
             * without doing anything
             */
            continue;
        }
        else if (event.type == EVENT_SIGNAL) {
            info(BHYEL "[*]" reset " Processing receipt of signal %d during single-step\n" reset, 
            event.u.signum);
            showTrace();
            ptrace(PTRACE_DETACH, pid, 0, event.u.signum);
            checkPtrace();
        }
        else {
            printf("Warning: unexpected event (%d) after break\n" reset, 
            event.type);
            showTrace();
            exit(-1);
        }
        return;
    }
    /* Finally single step past the ret instruction */
    singleStep();
}


/*
 * Await an event in the child.
 */
void awaitEvent(struct event *eventp){
    int status = 0;

    /* Wait for an event to occur on the child */
    errno = 0;
    while (waitpid(pid,&status,WUNTRACED | WCONTINUED) < 0){
        if (errno == ECHILD)
            err(BHRED "[-]" reset " Traced process disappeared.\n" reset) 
        else if (errno == EINTR){
                eventp->type = EVENT_UNKNOWN;
                continue;
            }
        else
            err(BHRED "[-]" reset " Wait\n" reset);
    }

    /* Child stopped because it encountered a breakpoint */
    if (WIFSTOPPED(status) && WSTOPSIG(status) == SIGTRAP){
        eventp->type = EVENT_BREAKPOINT;
        if(!firstEvent){
            idx = getTraceIdx(getPC() - 1);
            /* Check if u trigger a tracepoint */
            if(idx >= 0){
                setPC(getPC()-1);
                disableTracePoint(idx);
                /* What if the tracepoint is a breakpoint as well : dont continue */
                if(breakAddr == getPC()){
                    return;
                }
                /* Tracepoints may be triggered during single steps, dont continue in that case */
                if(strcmp(cmdArgs[0],"ss")) cont(false);
                return;
            }
            /* We did not encounter a tracepoint */
            else if(getBreakIdx(breakAddr) != -1){
                return;
            }
            return;
        }
        info(BHCYN "[*]" reset " Program stopped due to Reason : " BHYEL "Breakpoint\n" reset);
        return;
    }

    /* Child stopped because it received a signal */
    if (WIFSTOPPED(status) && WSTOPSIG(status) != SIGTRAP){
        eventp->type = EVENT_SIGNAL;
        eventp->u.signum = WSTOPSIG(status);
        info(BHYEL "[*]" reset " Signal Detected %s\n" reset,
             sys_siglist[eventp->u.signum]);
        context(1,1,1);
        showTrace();
        exit(-1);
    }

    /* Child terminated normally */
    if (WIFEXITED(status)){
        eventp->type = EVENT_EXIT;
        eventp->u.retval = WEXITSTATUS(status);
        info(BHYEL "[*]" reset " Detected event: exit with return value of %d\n" reset,
             eventp->u.retval);
        showTrace();
        exit(-1);
        return;
    }

    /* Child terminated because it received a signal */
    if (WIFSIGNALED(status)){
        eventp->type = EVENT_EXIT_SIGNAL;
        eventp->u.signum = WTERMSIG(status);
        info(BHYEL "[*]" reset " Detected event: Pending delivery of terminating signal %s\n" reset,
             sys_siglist[eventp->u.signum]);
        context(1,1,1); 
        showTrace();
        return;
    }

    /* Something unknown happened */
    info(BHYEL "[*]" reset " Detected an unknown event\n" reset);
    eventp->type = EVENT_UNKNOWN;
    showTrace();
    return;
}

/*
 * processEvent - Respond to the most recent event in the child
 */
void processEvent(struct event *eventp){
    static int inSyscall = 0;

    /* 
     * The traced child has stopped because it called execve. Use
     * this opportunity to set breakpoints at the entry point
     * of main function.
     */

    if (firstEvent){
        firstEvent = 0;
        initBreakPointTable();
        /* Start tracing all static symbols */
        trace(true);
        setBreakPoint(entryPoint);
        info(BHYEL "[*]" reset " Continuing until the breakpoint\n" reset);
        // Continue until the entryPoint
        ptrace(PTRACE_CONT, pid, 0, 0);
        sleep(1);
        checkPtrace();
        info(BHYEL "[*]" reset " Breakpoint Hit\n" reset);
        /* We know that entry point is also a tracepoint, lets record it */
        disableTracePoint(getTraceIdx(getPC()-1));
        // Once the breakpoint is hit, we set PC back to its original address
        setPC(breakAddr);
        /* Libc should be loaded at entrypoint, we can now trace dynamic symbols */
        getLibcBase();
        trace(false);
        context(1,1,1);
        deleteBreakPoint(getBreakIdx(breakAddr));
    }

    switch (eventp->type){
        /* The traced child has been stopped by a breakpoint instruction */
        case EVENT_BREAKPOINT:
            /* Breakpoint hit due to a tracepoint or syscall probably */
            if(getBreakIdx(breakAddr) == -1){
                /* Check if we have triggered a trace point : it has already been handled */
                if(idx == -1){
                    /* Syscalls apart from the ones which create a new process are harmless
                     * We need to handle syscalls like fork by clearing all breakpoints 
                     * otherwire child will die with it if it executes one of the breakpoints 
                     * it inherited from the parent
                     */  
                    int sysnum = getSysnum();
                    if(!inSyscall){
                        if(sysName(sysnum)) { info(BHYEL "[*]" reset " Syscall : %s\n" reset,sysName(sysnum)); }
                        else{
                            /* We're most likely single stepping */
                            context(1,1,1);
                            return;
                        } 
                        inSyscall = 1;
                        if(isFork(sysnum))
                            disableAllBrkpts();
                    }
                    else{
                        inSyscall = 0;
                        if(isFork(sysnum))
                            enableAllBrkpts();
                    }
                    singleStep();
                    return;
                }
                return;
            }
            /* Handle the breakpoint here */
            else{
                info(BHYEL "[*]" reset " Breakpoint Hit\n" reset);
                setPC(breakAddr);
                deleteBreakPoint(getBreakIdx(breakAddr));
                ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
                sleep(0.7);
                //context(1);
                checkPtrace();
                
                /* 
                * This part is tricky. The next event the parent discovers is
                * that the child is stopped by either the completion of the
                * single-stepping operation OR the pending receipt of some
                * signal. The parent receives notification of exactly one of
                * these events, never both, via wait(). In either case
                * though, the parent reenables the breakpoint and then
                * restarts the child. If the child was stopped by pending
                * receipt of a signal, then we need to forward that signal
                * when we restart the child.
                */
                awaitEvent(eventp);
                if (eventp->type == EVENT_BREAKPOINT) {
                    /* event is most likely due to single step, we simply return 
                     * without doing anything
                     */
                    return;
                }
                else if (eventp->type == EVENT_SIGNAL) {
                    info(BHYEL "[*]" reset " Processing receipt of signal %d during single-step\n" reset, 
                    eventp->u.signum);
                    showTrace();
                    ptrace(PTRACE_DETACH, pid, 0, eventp->u.signum);
                    checkPtrace();
                }
                else {
                    printf("Warning: unexpected event (%d) after break\n" reset, 
                    eventp->type);
                    showTrace();
                    exit(-1);
                }
                return;
            }   
        /* The child was stopped by pending receipt of a signal */
        case EVENT_SIGNAL:
            info(BHYEL "[*]" reset " Received %s\n" reset, sys_siglist[eventp->u.signum]);
            ptrace(PTRACE_SYSCALL, pid, 0, eventp->u.signum);
            checkPtrace();
            return;

        /* The child was terminated by the receipt of a signal */
        case EVENT_EXIT_SIGNAL:
            info(BHRED "[-]" reset " Terminated after receiving %s\n" reset,
                   sys_siglist[eventp->u.signum]);
            showTrace();
            exit(-1);

        /* The child terminated by calling the exit function */
        case EVENT_EXIT:
            info(BHRED "[-]" reset " Terminated with exit status %d\n" reset,
                   eventp->u.retval);
            showTrace();
            exit(-1);
        case EVENT_UNKNOWN:
        case EVENT_NONE:
            info(BHYEL "[*]" reset " Warning: Processing unexpected event\n" reset);
            ptrace(PTRACE_SYSCALL, pid, 0, 0);
            checkPtrace();
            return;

        default:
            info(BHYEL "[*]" reset " Warning: Processing unknown event %d\n" reset, eventp->type);
            ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
            checkPtrace();
    }
}

void attach(char *pname){
    char **endptr = {NULL};
    int res, status = 0, signo = 0;
    pid_t childPID = 0;

    /* Check if process is already being debugged */
    if(isProcessAttached){
        info(BHRED "[-]" reset " Process already being debugged\n" reset);
        return;
    }

    /* 
    ** Set breakpoint at entry point and continue execution until there 
    **  finally hand over control to user for other operations.
    */

    /*  We now start by forking our debugger into 2 processes and trace the child */
    switch (childPID = fork()){
        case -1:
            err(BHRED "[-]" reset " Fork");

        /* in the child process, set it up for getting traced */
        case 0:
            ptrace(PTRACE_TRACEME, 0, 0, 0);
            checkPtrace();
            // until here, address space between parent and child is shared 
            /* Now we execute the binary, and create a new address space */
            execl(pname, pname, NULL);
            err(BHRED "[-]" reset " Something's wrong while spawning the target\n" reset);

        /* in the parent process, do the tracing part */
        default:
            /* We might now consider that process is being debugged */
            isProcessAttached = true;
            pid = childPID;
            info(BHBLU "[+]" reset " Attaching to pid " BHRED "%u\n" reset,pid);
            char cmd[sizeof READAUXVALFMT + 0x20];
            snprintf(cmd,sizeof READAUXVALFMT + 0x20,READAUXVALFMT,pid);
            sleep(1);
            readCmdOutput(cmd,32);
            entryPoint = strtoul(outBuf,endptr,10);
            getBinBase();
            if(!entryPoint){
                if(errno == EINVAL || errno == ERANGE)
                    err(BHRED "[-]" reset " Something's wrong while converting str to int");
            }
            if(isBinPIE()){
                /* Update the symbol tables */
                elfPtr->staticSymbolTable    += binBase;
                elfPtr->staticSymbolTableEnd += binBase;
                elfPtr->dynamicSymbolTable   += binBase;
                elfPtr->staticSymbolTableEnd += binBase;
            }
            awaitEvent(&event);
            processEvent(&event);
    }
}
