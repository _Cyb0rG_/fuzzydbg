#include "interface.h"

u64 entryPoint       = 0;
u64 entryPointOffset = 0;
Elf_obj *elfPtr;
Elf_obj *libcPtr;
Elf64_Ehdr *elfHeader;
Elf64_Shdr *shdr;
int fd = 0;
struct stat libcSbuf;

void readELFHeader64(){
    u64 size = sbuf.st_size;
    /* Read the entire elf header into elfHeader for basic elfinfo stuff */
    elfHeader = (Elf64_Ehdr *)calloc(size,1);
    assert(elfHeader != NULL);
    assert(lseek(fd, (off_t)0, SEEK_SET) == (off_t)0);
	if(read(fd, (void *)elfHeader, size ) == 0)
        err(BHRED "[-]" reset " File format not supported\n" reset);
    if(!isELF64())
        err(BHRED "[-]" reset " Given file is not an ELF\n" reset);

    /* Seek the fd back to start, we now map the entire elf into mmap region */
    assert(lseek(fd, (off_t)0, SEEK_SET) == (off_t)0);

    /* First map space for the Elf_obj struct */
    elfPtr = (Elf_obj *)calloc(sizeof(Elf_obj),1);
    if(!elfPtr) err(BHRED "[-] " reset " mem err\n");    
    elfPtr->mlen  = sbuf.st_size;
    elfPtr->maddr = mmap(NULL,elfPtr->mlen,PROT_READ,MAP_SHARED,fd,0);
    
    if(elfPtr->maddr == MAP_FAILED) err(BHRED "[-]" reset " mmap!");    
    elfPtr->ehdr = elfPtr->maddr;


    /* Read in the necessary symbol tables */
    shdr = (Elf64_Shdr *)(elfPtr->maddr + elfPtr->ehdr->e_shoff);
    for(int i =0; i < elfPtr->ehdr->e_shnum ; i++){
        if(shdr[i].sh_type == SHT_SYMTAB){ /* Static Symbol Table */
            elfPtr->staticSymbolTable = (Elf64_Sym *)(elfPtr->maddr + shdr[i].sh_offset);
            elfPtr->staticSymbolTableEnd = (Elf64_Sym *)((char *)elfPtr->staticSymbolTable 
                                    + shdr[i].sh_size);
            elfPtr->staticStrTable = (char *)(elfPtr->maddr + shdr[shdr[i].sh_link].sh_offset);
        }
        if(shdr[i].sh_type == SHT_DYNSYM){ /* Dynamic Symbol Table */
            elfPtr->dynamicSymbolTable =  (Elf64_Sym *)(elfPtr->maddr + shdr[i].sh_offset);
            elfPtr->dynamicSymbolTableEnd = (Elf64_Sym *)((char *)elfPtr->dynamicSymbolTable
                                    + shdr[i].sh_size);
            elfPtr->dynamicStrTable = (char *)(elfPtr->maddr + shdr[shdr[i].sh_link].sh_offset);
        }
    }
}

void readLibcHeader(){
    /* First map space for the Elf_obj struct */
    libcPtr = (Elf_obj *)calloc(sizeof(Elf_obj),1);
    if(!libcPtr) err(BHRED "[-] " reset " mem err\n");    
    libcPtr->mlen  = libcSbuf.st_size;
    libcPtr->maddr = mmap(NULL,libcPtr->mlen,PROT_READ,MAP_SHARED,libcFd,0);
    
    if(libcPtr->maddr == MAP_FAILED) err(BHRED "[-]" reset " mmap!");    
    libcPtr->ehdr = libcPtr->maddr;


    /* Read in the necessary symbol tables */
    shdr = (Elf64_Shdr *)(libcPtr->maddr + libcPtr->ehdr->e_shoff);
    for(int i =0; i < libcPtr->ehdr->e_shnum ; i++){
        if(shdr[i].sh_type == SHT_DYNSYM){ /* Dynamic Symbol Table */
            libcPtr->dynamicSymbolTable =  (Elf64_Sym *)(libcPtr->maddr + shdr[i].sh_offset);
            libcPtr->dynamicSymbolTableEnd = (Elf64_Sym *)((char *)libcPtr->dynamicSymbolTable
                                    + shdr[i].sh_size);
            libcPtr->dynamicStrTable = (char *)(libcPtr->maddr + shdr[shdr[i].sh_link].sh_offset);
        }
    }
}

bool isELF64() {
	if ((elfHeader->e_ident[EI_CLASS] == ELFCLASS64) && 
        !strncmp((char*)elfHeader->e_ident, "\177ELF", 4))
		return true;
	else
		return false;
}

void showHeader64(){
	/* Storage capacity class */
	info(BHYEL "[*]" reset " Storage class\t= ");
	switch(elfHeader->e_ident[EI_CLASS]){
		case ELFCLASS32:
			info("32-bit objects\n\n" reset);
			break;

		case ELFCLASS64:
			info("64-bit objects\n\n" reset);
			break;

		default:
			info("INVALID CLASS\n\n" reset);
			break;
	}

	/* Data Format */
	info(BHYEL "[*]" reset " Data format\t= ");
	switch(elfHeader->e_ident[EI_DATA]){
		case ELFDATA2LSB:
			info("2's complement, little endian\n\n" reset);
			break;

		case ELFDATA2MSB:
			info("2's complement, big endian\n\n" reset);
			break;

		default:
			info("INVALID Format\n\n" reset);
			break;
	}

	/* ELF filetype */
	info(BHYEL "[*]" reset " Filetype \t= ");
	switch(elfHeader->e_type){
		case ET_NONE:
			info("N/A (0x0)\n\n" reset);
			break;

		case ET_REL:
			info("Relocatable\n\n" reset);
			break;

		case ET_EXEC:
			info("Executable\n\n" reset);
			break;

		case ET_DYN:
			info("Shared Object\n\n" reset);
			break;
		default:
			info("Unknown (0x%x)\n\n" reset, elfHeader->e_type);
			break;
	}

	/* Entry point */
	info(BHYEL "[*]" reset " Entry point\t= 0x%08lx\n\n" reset, elfHeader->e_entry);

	/* ELF header size in bytes */
	info("ELF header size\t= 0x%08x\n\n" reset, elfHeader->e_ehsize);

	/* Program Header */
	info("\nProgram Header\t= ");
	info("0x%08lx\n" reset, elfHeader->e_phoff);		/* start */
	info("\t\t  %d entries\n" reset, elfHeader->e_phnum);	/* num entry */
	info("\t\t  %d bytes\n" reset, elfHeader->e_phentsize);	/* size/entry */

	/* Section header starts at */
	info("\nSection Header\t= ");
	info("0x%08lx\n" reset, elfHeader->e_shoff);		/* start */
	info("\t\t  %d entries\n" reset, elfHeader->e_shnum);	/* num entry */
	info("\t\t  %d bytes\n" reset, elfHeader->e_shentsize);	/* size/entry */
	info("\t\t  0x%08x (string table offset)\n" reset, elfHeader->e_shstrndx);
	info("\n" reset);	/* End of ELF header */
}

bool isBinPIE(){
    return elfHeader->e_type == ET_DYN;
}

/*
 * staticSymName - Return ASCII name of a static symbol
 */
char *staticSymName(Elf64_Sym *sym){
    return &elfPtr->staticStrTable[sym->st_name];
}

/*
 * dynSymName - Return ASCII name of a dynamic symbol
 */ 
char *dynSymName(Elf64_Sym *sym){
    return &elfPtr->dynamicStrTable[sym->st_name];
}

/*
 * firstStaticSym - Return ptr to first symbol in static symbol table
 */
Elf64_Sym *firstStaticSym(){
    return elfPtr->staticSymbolTable;
}

/*
 * nextStaticSym - Return ptr to next symbol in static symbol table,
 * or NULL if no more symbols.
 */
Elf64_Sym *nextStaticSym(Elf64_Sym *sym){
    sym++;
    if (sym < elfPtr->staticSymbolTableEnd)
	    return sym;
    else
	    return NULL;
}

/*
 * firstDynSym - Return ptr to first symbol in dynamic symbol table
 */
Elf64_Sym *firstDynSym(){
    return libcPtr->dynamicSymbolTable;
}

/*
 * nextDynSym - Return ptr to next symbol in dynamic symbol table,
 * of NULL if no more symbols.
 */ 
Elf64_Sym *nextDynSym(Elf64_Sym *sym){
    sym++;
    if (sym < libcPtr->dynamicSymbolTableEnd)
	    return sym;
    else
	    return NULL;
}

/*
 * isStaticFunc - Return true if symbol is a static function
 */
bool isStaticFunc(Elf64_Sym *sym) {
    return ((ELF64_ST_TYPE(sym->st_info) == STT_FUNC) &&
	    (sym->st_shndx != SHT_NULL));
}

/*
 * isDynFunc - Return true if symbol is a dynamic function 
 */
bool isDynFunc(Elf64_Sym *sym) {
    return ((ELF64_ST_TYPE(sym->st_info) == STT_FUNC));
}