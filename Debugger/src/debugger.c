#include "interface.h"

void context(bool regs,bool disas,bool stack){
    if(regs){
        info(BHMAG "\n-------------REGS-----------\n" reset);
        info(BHCYN "RAX : " reset "0x%lx\n" reset, getRegs("rax"));
        info(BHCYN "RBX : " reset "0x%lx\n" reset, getRegs("rbx"));
        info(BHCYN "RCX : " reset "0x%lx\n" reset, getRegs("rcx"));
        info(BHCYN "RDX : " reset "0x%lx\n" reset, getRegs("rdx"));
        info(BHCYN "RIP : " reset "0x%lx\n" reset, getRegs("rip"));
        info(BHCYN "RSP : " reset "0x%lx\n" reset, getRegs("rsp"));
        info(BHCYN "RDI : " reset "0x%lx\n" reset, getRegs("rdi"));
        info(BHCYN "RSI : " reset "0x%lx\n" reset, getRegs("rsi"));
        info(BHCYN "R8  : " reset "0x%lx\n" reset, getRegs("r8"));
        info(BHCYN "R9  : " reset "0x%lx\n" reset, getRegs("r9"));
        info(BHCYN "R10 : " reset "0x%lx\n" reset, getRegs("r10"));
        info(BHCYN "R11 : " reset "0x%lx\n" reset, getRegs("r11"));
        info(BHCYN "R12 : " reset "0x%lx\n" reset, getRegs("r12"));
        info(BHCYN "R13 : " reset "0x%lx\n" reset, getRegs("r13"));
        info(BHCYN "R14 : " reset "0x%lx\n" reset, getRegs("r14"));
        info(BHCYN "R15 : " reset "0x%lx\n" reset, getRegs("r15"));
    }
        info(BHMAG "----------------------------\n" reset);
    if(disas){
        info(BHBLU "---------Disassembly--------\n" reset);
        disassemble(getRegs("rip"));
        info(BHBLU "----------------------------\n" reset);
    }
    if(stack){
        info(BHMAG "\n-------------STACK-----------\n" reset);
        for(int i=0; i<0x8 ;i++){
            info(BHBLU "0x%lx" reset  "| +%d: 0x%lx\n",getRegs("rsp") + i*8,i,getValue("rsp",i*8));
        }
        info(BHBLU "----------------------------\n" reset);
    }
    checkPtrace();
}

void show(){
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process attached\n" reset);
        return;
    }
    if(cmdArgs[1] == NULL){
        info(BHRED "[-]" reset " show <state/asm/regs>\n");
        return;
    }
    /* for symbols */
    if(!strcmp(cmdArgs[1],"sym")){
        if(cmdArgs[2] == NULL){
            info(BHRED "[-]" reset " show sym <symbol name>\n" reset);
            return;
        }
        /* Iterate through tracetable to print symbol name */
        for(int i = 0; traceTable[i].addr != NULL; i++){
            char *staticName = elfPtr->staticStrTable + traceTable[i].sym->st_name;
            char *dynName    = libcPtr->dynamicStrTable + traceTable[i].sym->st_name;
            if(!strcmp(cmdArgs[2],staticName)){
                info(BHYEL "[+]" reset " %s : 0x%lx\n",staticName,traceTable[i].sym->st_value);
                return;
            }
            else if(!strcmp(cmdArgs[2],dynName)){
                info(BHYEL "[+]" reset " %s : 0x%lx\n",dynName,traceTable[i].sym->st_value + libcBase);
                return;
            }
        }
    }
    if(!strcmp(cmdArgs[1],"syms")){
        for(int i = 0; traceTable[i].addr != NULL; i++){
            char *staticName = elfPtr->staticStrTable + traceTable[i].sym->st_name;
            char *dynName    = libcPtr->dynamicStrTable + traceTable[i].sym->st_name;
            if(traceTable[i].symType == STATIC)
                info(BHBLU "[*]" reset " %s : 0x%lx\n",staticName,traceTable[i].sym->st_value);
            if(traceTable[i].symType == DYNAMIC)
                info(BHBLU "[+]" reset " %s : 0x%lx\n",dynName,traceTable[i].sym->st_value + libcBase);
        }
    }
    if(!strncmp(cmdArgs[1],"state",strlen("state")))   context(1,1,1);
    if(!strncmp(cmdArgs[1],"regs" ,strlen("regs")))    context(1,0,0);
    if(!strncmp(cmdArgs[1],"asm"  ,strlen("asm")))     context(0,1,0);
    if(!strncmp(cmdArgs[1],"stack",strlen("stack")))   context(0,0,1);
    if(!strncmp(cmdArgs[1],"map"  ,strlen("map")))     showMaps();
    if(!strncmp(cmdArgs[1],"trace",strlen("trace")))   showTrace();
}


/* Fetch syscall number */
int getSysnum(){ 
    long res = ptrace(PTRACE_PEEKUSER, pid, 8*ORIG_RAX, 0);
    checkPtrace();
    return res;
}
/*
 * isFork - Return true if syscall sysnum creates a new process.
 */
int isFork(int sysnum){
    return (0 || (sysnum == __NR_fork) || (sysnum == __NR_clone) || 
	    (sysnum == __NR_vfork));
}

/*
 * setPC - Modify the program counter for the traced process
 */
void setPC(void *addr){
    ptrace(PTRACE_POKEUSER, pid, 8*RIP, addr);
    sleep(0.4);
    checkPtrace();
}

/* getPC - Fetch current program counter for the traced process */
void *getPC() {
    long* res =  (void *)ptrace(PTRACE_PEEKUSER, pid, RIP*8, 0);
    checkPtrace();
    return res;
}

u64 getValue(char *reg,u64 offset){
    if(!isProcessAttached) err(BHRED "[-]" reset " No such proces\n");
    if(!reg) return -1;
    else{
        u64 val = ptrace(PTRACE_PEEKTEXT,pid,getRegs(reg) + offset,0);
        checkPtrace();
        return val;
    }
}

void setValue(char *reg,u64 val){
    if(!isProcessAttached) err(BHRED "[-]" reset " No such proces\n");
    if(!reg) return;
    else{
        ptrace(PTRACE_POKETEXT,pid,getRegs(reg),val);
        checkPtrace();
    }
}

void disassemble(ull vaddr){
    // Initialize decoder context
    ZydisDecoder decoder;
    ZydisDecoderInit(&decoder, ZYDIS_MACHINE_MODE_LONG_64, ZYDIS_STACK_WIDTH_64);

    // Initialize formatter. Only required when you actually plan to do instruction
    // formatting ("disassembling"), like we do here
    ZydisFormatter formatter;
    ZydisFormatterInit(&formatter, ZYDIS_FORMATTER_STYLE_INTEL);
    /* Attempt to disassemble from RIP */
    u64 buffer = ptrace(PTRACE_PEEKDATA,pid,(void *)vaddr,0);
    ZyanU8 * data = (ZyanU8 *)&buffer;
    checkPtrace();
    // Loop over the instructions in our buffer.
    // The runtime-address (instruction pointer) is chosen arbitrary here in order to better
    // visualize relative addressing
    ZyanU64 runtime_address = vaddr;
    ZyanUSize offset = 0;
    const ZyanUSize length = sizeof(data);
    ZydisDecodedInstruction instruction;
    ZydisDecodedOperand operands[ZYDIS_MAX_OPERAND_COUNT_VISIBLE];
    while (ZYAN_SUCCESS(ZydisDecoderDecodeFull(&decoder, data + offset, length - offset,
        &instruction, operands, ZYDIS_MAX_OPERAND_COUNT_VISIBLE, 
        ZYDIS_DFLAG_VISIBLE_OPERANDS_ONLY))){
        // Print current instruction pointer.
        printf("%016" PRIX64 "  ", runtime_address);

        // Format & print the binary instruction structure to human readable format
        char buffer[256];
        ZydisFormatterFormatInstruction(&formatter, &instruction, operands,
            instruction.operand_count_visible, buffer, sizeof(buffer), runtime_address);
        puts(buffer);
        offset += instruction.length;
        runtime_address += instruction.length;
    }
}

void showMaps(){
    if(!isProcessAttached){
        info(BHRED "[-]" reset " No process attached\n");
    }
    int i = 0;
    char cmd[0x40];
    char **endptr = NULL;
    snprintf(cmd,0x40,PROCMAPSFMT,pid);
    FILE* map = fopen(cmd,"r");
    assert(map != NULL);
    char *data = malloc(0x4000);
    while(!feof(map)){
        if(fgets(data,PROCMAPS_LINE_MAX_LENGTH,map) == NULL){
            if(feof(map)) break;
            else err(BHRED "[-]" reset " Something wrong while parsing maps\n");
        }
            
        info("%s",data);
    } 
}