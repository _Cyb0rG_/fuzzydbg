#include "interface.h"

int crashes = 0;
char *mutatedBlob;

struct ORIGINAL_FILE getData(char* seed) {
    /* We try to read the target via this function */
    FILE *target = fopen(seed, "rb");
    if (target == NULL) err(BHRED "[-]" reset " Unable to open the seed file\n" reset);
    /* seek to the end of file */
    fseek(target, 0, SEEK_END);
    /* Get the length */
    u64 filelen = ftell(target);
    rewind(target);
    info(BHBLU "[+]" reset " File length : " BHRED "0x%lx\n" reset,filelen);
    // Allocate enough memory
    char *cloneData = (char *)malloc(filelen * sizeof(char));

    // Read in the data
    if(fread(cloneData, filelen, 1, target)) fclose(target);
    else err(BHRED "[-]" reset " Unable to read the target\n" reset);

    struct ORIGINAL_FILE origFile = {cloneData,filelen};
    return origFile;
}

void mutateBits(int iteration,struct ORIGINAL_FILE origFile, size_t mutations) {

    /*
    / ----------------MUTATE THE BITS-------------------------
    */
    u64 *pickedIndices = (u64 *)malloc(sizeof(u64)*mutations);
    if(!pickedIndices) err(BHRED "[-]" reset " Mem alloc error\n" reset);
    
    /* seed : abcd, mutation = 2 , iter = 1 -> 
    /* Initialize the random indices to be picked for mutation */
    for (int i = 0; i < (int)mutations; i++)
        pickedIndices[i] = rand() % origFile.length; // 0 2

    /* Try to increment number of bits being added to the mutated blob */
    mutatedBlob = (char *)malloc(origFile.length + iteration*2);
    if(!mutatedBlob) err(BHRED "[-]" reset " Mem alloc error\n" reset);
    /* Copy the data of seed file into a buffer */
    memcpy(mutatedBlob, origFile.data, origFile.length);

    /* Now flip random bits in the seed according to the picked indices */
    for (int i = 0; i < (int)mutations; i++) {
        char current = mutatedBlob[pickedIndices[i]];

        // figure out what bit to flip in this 'decimal' byte
        int randByte = rand() % 256;
        
        mutatedBlob[pickedIndices[i]] = (char)randByte;
    }

    /* Generate random bytes to add to the end of mutated blob */
    for(int i = 0; i < iteration ; i++){
        int randByte = rand() % 256;
        mutatedBlob[origFile.length + i] = randByte;
    }
    /*
    / ---------WRITING THE MUTATED BITS TO NEW FILE-----------
    */
    FILE *target = fopen("seed", "wb");
    if (target == NULL) err(BHRED "[-]" reset " Unable to create a mutation file\n" reset);
    
    /* Put the mutated blob in the mutation file */
    fwrite(mutatedBlob, 1, origFile.length, target);
    /* Also write the added bytes */
    fwrite(mutatedBlob + origFile.length,1,iteration,target);
    /* Close the mutation file and free necessary mem */ 
    fclose(target);
    free(mutatedBlob);
    free(pickedIndices);
}

void start(char **argv,int iteration) {
    pid_t childPID;
    int childStatus;
    char command[50] = {"\x00"};

    childPID = fork();
    if (childPID == 0) {
        
        /* Open the seed and redirect stdin to take input from seed */
        int fd = open(cmdArgs[1], O_WRONLY);
        dup2(fd, 0);  // 0 - > stdin     
        /* In child , we execve the target */
        execl(argv[1],argv[1],cmdArgs[1],NULL);
        /* shouldn't return, if it does, we have an error */
        err(BHRED "[-]" reset " Something wrong while spawning fuzz target\n" reset);
    }
    else {
        /* In parent,  we trace child for any crashes */
        do {
            pid_t tpid = waitpid(childPID, &childStatus, WUNTRACED | WCONTINUED);
            if (tpid == -1) err(BHRED "[-]" reset " waitpid failed\n" reset);
            if (WIFEXITED(childStatus)) {
                info(BHYEL "[*]" reset " WIFEXITED: Exit Status: %d\n" reset, WEXITSTATUS(childStatus));
            } else if (WIFSIGNALED(childStatus)) {
                crashes++;
                int exitStat = WTERMSIG(childStatus);
                info("\r[>] Crashes: %d\n" reset, crashes);
                fflush(stdout);
                snprintf(command, sizeof command,"cp ./seed crashes/%d.%d", iteration, 
                exitStat);
                // VERY UNSECURE - TODO : Reimplement this with generic file trick
                system(command);
            } else if (WIFSTOPPED(childStatus)) {
                printf("WIFSTOPPED: Exit Status: %d\n" reset, WSTOPSIG(childStatus));
            } else if (WIFCONTINUED(childStatus)) {
                printf("WIFCONTINUED: Exit Status: Continued.\n" reset);
            }
        } while (!WIFEXITED(childStatus) && !WIFSIGNALED(childStatus));
    }
}

void fuzz(char **argv) {
    /* Generate a random seed
     * TODO : Generate better random seed
     */
    srand((unsigned)time(NULL));
    if(!cmdArgs[1] || !cmdArgs[2]){
        info(BHYEL "[*]" reset " Usage : fuzz <seed> <iters>\n" reset);
        return;
    }
    /* Fancy mutation creation - read the seed file */
    struct ORIGINAL_FILE origFile = getData(cmdArgs[1]);
    size_t mutations = (origFile.length + 0x10) * 0.2;
    info("[>] Flipping and mutating up to %ld bytes.\n" reset, mutations);

    int iterations = atoi(cmdArgs[2]);
    if(iterations < 0 || iterations > 1000){
        info(BHRED "[-]" reset " Iteration num not supported\n" reset);
        return;
    }
    info("[>] Fuzzing for %d iterations...\n" reset, iterations);

    /* Measure the time for fuzzing also */
    clock_t t = clock();
    for (int i = 0; i < iterations; i++) {
        mutateBits(i,origFile, mutations);
        start(argv,i);
    }
    t = clock() - t;
    double timeTaken = ((double)t)/CLOCKS_PER_SEC;
    info("\n[>] Completed %d fuzz cases in %f seconds\n" reset, iterations, timeTaken);
    return;
}